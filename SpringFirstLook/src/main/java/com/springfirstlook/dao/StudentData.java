package com.springfirstlook.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.springfirstlook.model.StudentModel;

@Component
public class StudentData {
	List<StudentModel> data = new ArrayList<>();

	public StudentModel createStu(StudentModel stu) {
		data.add(stu);
		return stu;
	}

	public StudentModel getStuData(int id) {

		for (StudentModel i : data) {
			if (i.getId() == id) {
				return i;
			}
		}
		return null;
	}

	public List getAllData(StudentModel stu) {

		return data;
	}

	public StudentModel updateStudent(int id, StudentModel stu) {

		for (StudentModel i : data) {
			if (i.getId() == id) {
				i.setName(stu.name);
				i.setAge(stu.age);
				i.setSchool(stu.school);
				i.setSec(stu.sec);
				return i;
			}
		}
		return null;
	}

	public List deleteData(int id) {

		for (StudentModel i : data) {
			if (i.getAge() == id) {
				data.remove(i);
			}
		}
		return data;
	}

	public String deleteAllData() {
		boolean Status = data.removeAll(data);
		if (Status) {
			return "Deleted Successfully";
		} else {
			return "Something happend...not deleted";
		}
	}
}
