package com.springfirstlook.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.springfirstlook.dao.StudentData;
import com.springfirstlook.model.StudentModel;

@Component
public class StudentService {

	@Autowired
	StudentData stuData;

	public StudentModel get(int id) {
		return stuData.getStuData(id);
	}

	public StudentModel create(StudentModel stu) {
		return stuData.createStu(stu);
	}

	public List getAll(StudentModel stu) {
		return stuData.getAllData(stu);
	}

	public StudentModel update(int id, StudentModel stu) {
		return stuData.updateStudent(id, stu);
	}

	public List delete(int id) {
		return stuData.deleteData(id);
	}

	public String deleteAll() {
		return stuData.deleteAllData();
	}

}
