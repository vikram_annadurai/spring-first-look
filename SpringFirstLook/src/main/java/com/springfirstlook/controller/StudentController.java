package com.springfirstlook.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springfirstlook.model.StudentModel;
import com.springfirstlook.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {
	@Autowired
	StudentService stuService;

	@GetMapping("/getstudent/{id}")
	public StudentModel getStudent(@PathVariable int id) {
		return stuService.get(id);
	}

	@PostMapping("/createstudent")
	public StudentModel createStudent(@RequestBody StudentModel stu) {
		return stuService.create(stu);
	}

	@GetMapping("/getallstudent")
	public List getAllStudent(StudentModel stu) {
		return stuService.getAll(stu);
	}

	@PutMapping("/updatestudent/{id}")
	public StudentModel updateStudent(@PathVariable int id, @RequestBody StudentModel stu) {
		return stuService.update(id, stu);
	}

	@DeleteMapping("/deletestudent/{id}")
	public List delete(@PathVariable int id) {
		return stuService.delete(id);
	}

	@DeleteMapping("/deleteallstudent")
	public String deleteAll() {
		return stuService.deleteAll();
	}
}
