package com.springfirstlook.model;

public class StudentModel {
	
	public int id;
	public String sec;
	public String name;
	public int age;
	public String school;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSec() {
		return sec;
	}
	public void setSec(String sec) {
		this.sec = sec;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	@Override
	public String toString() {
		return "StudentModel [id=" + id + ", sec=" + sec + ", name=" + name + ", age=" + age + ", school=" + school
				+ "]";
	}
	
	
	
	
	
	
}
